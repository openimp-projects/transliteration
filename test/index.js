const tr  = require('./../index');

describe('tr', () => {
  describe('standard a-z characters', () => {
    describe('without flags', () => {
      it('should produce a function that transliterates abcd to dcba when search is abcd and replacement is dcba', () => {
        const expected = 'dcba';
        const actual = tr('abcd', 'abcd', 'dcba');
        expect(actual).to.be.equal(expected);
      });
      it('should produce a function that transliterates abcdd to dcbaa when search is abcd and replacement is dcba', () => {
        const expected = 'dcbaa';
        const actual = tr('abcdd', 'abcd', 'dcba');
        expect(actual).to.be.equal(expected);
      });
      it('should produce a function that transliterates abcd to aaaa when search is abcd and replacement is a', () => {
        const expected = 'aaaa';
        const actual = tr('abcd', 'abcd', 'a');
        expect(actual).to.be.equal(expected);
      });
      it('should produce a function that transliterates abba to poop when search is ab and replacement is po', () => {
        const expected = 'poop';
        const actual = tr('abba', 'ab', 'po');
        expect(actual).to.be.equal(expected);
      });
      it('should produce a function that transliterates xxx to d when search is xxx and replacement is dcf', () => {
        const expected = 'ddd';
        const actual = tr('xxx', 'xxx', 'dcf');
        expect(actual).to.be.equal(expected);
      });
      it('should produce a function that transliterates ruby to perl when search is bury and replacement is repl', () => {
        const expected = 'perl';
        const actual = tr('ruby', 'bury', 'repl');
        expect(actual).to.be.equal(expected);
      });
    });
    describe('with flags', () => {
      let flags;
      describe('with s flag', () => {
        before(() => {
          flags = 's';
        });
        it('should produce a function that transliterates abba to pop when search is ab and replacement is pop', () => {
          const expected = 'pop';
          const actual = tr('abba', 'ab', 'po', flags);
          expect(actual).to.be.equal(expected);
        });
        it('should produce a function that transliterates abba to aba when search is b and replacement is null', () => {
          const expected = 'aba';
          const actual = tr('abba', 'b', '', flags);
          expect(actual).to.be.equal(expected);
        });
        it('Should be greedy when squashing replaced characters', () => {
          const expected = 'aaa';
          const actual = tr('abbba', 'b', 'a', 's');
          expect(actual).to.equal(expected);
        });
        it('Should handle squashing characters with multiple source characters', () => {
          const expected = 'aaa';
          const actual = tr('abccbba', 'bc', 'a', 's');
          expect(actual).to.equal(expected);
        });
      });
      describe('with d flag', () => {
        before(() => {
          flags = 'd';
        });
        it('should produce a function that transliterates abba to aa when search is b and replacement is null', () => {
          const expected = 'aa';
          const actual = tr('abba', 'b', '', flags);
          expect(actual).to.be.equal(expected);
        });
        it('should produce a function that transliterates abcd to cba when search is abcd and replacement is cba', () => {
          const expected = 'cba'
          const actual = tr('abcd', 'abcd', 'cba', flags);
          expect(actual).to.be.equal(expected);
        });
        it('should produce a function that transliterates abcd to a when search is abcd and replacement is a', () => {
          const expected = 'a';
          const actual = tr('abcd', 'abcd', 'a', flags);
          expect(actual).to.be.equal(expected);
        });
        it('should produce a function that transliterates adam to eve when search is adm and replacement is ev', () => {
          const expected = 'eve';
          const actual = tr('adam', 'adm', 'ev', flags);
          expect(actual).to.be.equal(expected);
        });
      });
      describe('with ds flags', () => {
        before(() => {
          flags = 'ds';
        });
        it('should produce a function that transliterates abba to p when search is ab and replacement is p', () => {
          const expected = 'p';
          const actual = tr('abba', 'ab', 'p', flags);
          expect(actual).to.be.equal(expected);
        });
      });
    });
  });
  describe('characters that would need escaping e.g. "()[]{}..."', () => {
    describe('without flags', () => {
      it('should produce a function that transliterates ( to ) when search [({< is and replacement is ])}>', () => {
        const expected = ')';
        const actual = tr('(', '[({<', '])}>');
        expect(actual).to.be.equal(expected);
      });
      it('should produce a function that transliterates ()abc to [)qbc when search is (a and replacement is [q', () => {
        const expected = '[)qbc';
        const actual = tr('()abc', '(a', '[q');
        expect(actual).to.be.equal(expected);
      });
      it('should produce a function that transliterates d*d to dad when search is * and replacement is a', () => {
        const expected = 'dad';
        const actual = tr('d*d', '*', 'a');
        expect(actual).to.be.equal(expected);
      });
      it('should produce a function that transliterates a-z to a+z when search is - and replacement is +', () => {
        const expected = 'a+z';
        const actual = tr('a-z', '-', '+');
        expect(actual).to.be.equal(expected);
      });
      it('should produce a function that transliterates ()a to ((a when search is [](){}<> and replacement is [[(({{<<', () => {
        const expected = '((a';
        const actual = tr('()a', '[](){}<>', '[[(({{<<');
        expect(actual).to.be.equal(expected);
      });
    });
    describe('with flags', () => {
      let flags;
      describe('with s flag', () => {
        beforeEach(() => {
          flags = 's';
        });
        it('should produce a function that transliterates () to ( when search is [](){}<> and replacement is [[(({{<<', () => {
          const expected = '(';
          const actual = tr('()', '[](){}<>', '[[(({{<<', flags);
          expect(actual).to.be.equal(expected);
        });
      });
      describe('with d flag', () => {
        beforeEach(() => {
          flags = 'd';
        });
        it('should produce a function that transliterates ()[] to ){} when search is []( and replacement is {}', () => {
          const expected = '){}';
          const actual = tr('()[]', '[](', '{}', flags);
          expect(actual).to.be.equal(expected);
        });
      });
      describe('with ds flags', () => {
        beforeEach(() => {
          flags = 'ds';
        });
        it('should produce a function that transliterates ()a to (a when search is [](){}<> and replacement is [[(({{<<', () => {
          const expected = '(a';
          const actual = tr('()a', '[](){}<>', '[[(({{<<', flags);
          expect(actual).to.be.equal(expected);
        });
        it('should produce a function that transliterates ()a to ( when search is [](){}<>\\0-\\377 and replacement is [[(({{<<', () => {
          const expected = '(';
          const actual = tr('()a', '[](){}<>\\0-\\377', '[[(({{<<', flags);
          expect(actual).to.be.equal(expected);
        });
        it('Should handle multiple locations for squashed characters', () => {
          const expected = '(aa(';
          const actual = tr('()aa))', '()', '((', 'ds');
          expect(actual).to.equal(expected);
        });
      });
    });
  });
});
