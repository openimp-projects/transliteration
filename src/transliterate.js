function tr(text, search, replace, flags = '') {
  const escaped = _escapeRequiredChars(search);

  let lastReplacedChr = '';
  let lastReplacedEnd = 0;

  return text.replace(new RegExp(`([${escaped}])\\1*`, 'g'), (chars, _, offset) => {
    if (lastReplacedEnd < offset) {
      lastReplacedChr = '';
    }

    lastReplacedEnd = offset + chars.length;

    const replaceIndex = search.indexOf(chars[0]);
    let replacement = replace[replaceIndex];

    if (!replacement) {
      if (flags.includes('d')) {
        return '';
      }
      replacement = replace[replace.length - 1] || '';
    }

    if (lastReplacedChr === replacement && flags.includes('s')) {
      if (replacement === '') {
        return chars[0];
      } else {
        return '';
      }
    }

    lastReplacedChr = replacement;

    const returnCount = flags.includes('s') ? 1 : chars.length;
    return replacement.repeat(returnCount);
  });
}

const _escapeRequiredChars = s => s.replace(/[\/^$*+?.()|[\]{}]/g, '\\$&');

module.exports = tr;
